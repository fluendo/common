TARGET_DIR=$(HTML_DIR)/$(DOC_MODULE)

if ENABLE_GTK_DOC
all:
	gtkdoc-scan --ignore-headers="$(IGNORE_HFILES)" --source-dir=$(DOC_SOURCE_DIR) --module=$(DOC_MODULE) $(SCAN_OPTIONS) $(EXTRA_HFILES)
	gtkdoc-mkdb --output-format=xml --ignore-files="$(IGNORE_HFILES)" --source-dir=$(DOC_SOURCE_DIR) --module=$(DOC_MODULE)
	mkdir -p html
	if test ! -f $(DOC_MAIN_SGML_FILE) ; then \
		cp $(abs_srcdir)/$(DOC_MAIN_SGML_FILE) . ; \
	fi;
	cd html && gtkdoc-mkhtml $(DOC_MODULE) ../$(DOC_MAIN_SGML_FILE)

	-@test "x$(HTML_IMAGES)" = "x" || \
	for file in $(HTML_IMAGES) ; do \
	  if test -f $(abs_srcdir)/$$file ; then \
	    cp $(abs_srcdir)/$$file $(abs_builddir)/html; \
	  fi; \
	  if test -f $(abs_builddir)/$$file ; then \
	    cp $(abs_builddir)/$$file $(abs_builddir)/html; \
	  fi; \
	done;

endif

clean-local:
	rm -f *.bak
	rm -f *.txt
	rm -f *.types
	rm -f *.stamp
	find xml ! -name '*.ent*' -type f -exec rm -f {} +
	rm -rf  html

distclean-local:
	rm -f *.bak
	rm -f *.txt
	rm -f *.types
	rm -f *.stamp
	find xml ! -name '*.in' -type f -exec rm -f {} +
	rm -rf html

maintainer-clean-local: distclean-local

install-data-local: all
	@installfiles=`echo $(builddir)/html/*`; \
	if test "$$installfiles" = '$(builddir)/html/*'; \
	then echo 1>&2 'Nothing to install' ; \
	else \
	  if test -n "$(DOC_MODULE_VERSION)"; then \
	    installdir="$(DESTDIR)$(TARGET_DIR)-$(DOC_MODULE_VERSION)"; \
	  else \
	    installdir="$(DESTDIR)$(TARGET_DIR)"; \
	  fi; \
	  $(mkinstalldirs) $${installdir} ; \
	  for i in $$installfiles; do \
	    echo ' $(INSTALL_DATA) '$$i ; \
	    $(INSTALL_DATA) $$i $${installdir}; \
	  done; \
	  if test -n "$(DOC_MODULE_VERSION)"; then \
	    mv -f $${installdir}/$(DOC_MODULE).devhelp2 \
	      $${installdir}/$(DOC_MODULE)-$(DOC_MODULE_VERSION).devhelp2; \
	  fi; \
	  $(GTKDOC_REBASE) --relative --dest-dir=$(DESTDIR) --html-dir=$${installdir}; \
	fi

uninstall-local:
	@if test -n "$(DOC_MODULE_VERSION)"; then \
	  installdir="$(DESTDIR)$(TARGET_DIR)-$(DOC_MODULE_VERSION)"; \
	else \
	  installdir="$(DESTDIR)$(TARGET_DIR)"; \
	fi; \
	rm -rf $${installdir}
