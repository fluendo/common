dnl -*- mode: autoconf -*-

# serial 2

dnl Usage:
dnl   GTK_DOC_CHECK([minimum-gtk-doc-version])
AC_DEFUN([GTK_DOC_CHECK],
[
  AC_REQUIRE([PKG_PROG_PKG_CONFIG])

  dnl check for gtk-doc
  AC_CHECK_PROG(have_gtkdoc_scan,gtkdoc-scan,yes,no)
  if test "x$have_gtkdoc_scan" = "xyes"; then
    AC_CHECK_PROG(have_gtkdoc_mkdb,gtkdoc-mkdb,yes,no)
    if test "x$have_gtkdoc_mkdb" = "xyes"; then
      AC_CHECK_PROG(have_gtk_doc,gtkdoc-mkhtml,yes,no)
    fi
  fi

  dnl check for tools we added during development
  AC_PATH_PROGS([GTKDOC_REBASE],[gtkdoc-rebase],[true])

  dnl for overriding the documentation installation directory
  AC_ARG_WITH([html-dir],
    AS_HELP_STRING([--with-html-dir=PATH], [path to installed docs]),,
    [with_html_dir='${datadir}/gtk-doc/html'])
  HTML_DIR="$with_html_dir"
  AC_SUBST([HTML_DIR])

  dnl enable/disable documentation building
  AC_ARG_ENABLE([gtk-doc],
    AS_HELP_STRING([--enable-gtk-doc],
                   [use gtk-doc to build documentation [[default=no]]]),,
    [enable_gtk_doc=no])

  AC_MSG_CHECKING([whether to build gtk-doc documentation])
  AC_MSG_RESULT($enable_gtk_doc)

  if test "x$enable_gtk_doc" = "xyes" && test "$have_gtk_doc" = "no"; then
    AC_MSG_ERROR([
  You must have gtk-doc installed to build documentation for
  $PACKAGE_NAME. Please install gtk-doc or disable building the
  documentation by adding '--disable-gtk-doc' to '[$]0'.])
  fi

  AM_CONDITIONAL([HAVE_GTK_DOC], [test x$have_gtk_doc = xyes])
  AM_CONDITIONAL([ENABLE_GTK_DOC], [test x$enable_gtk_doc = xyes])
  AM_CONDITIONAL([GTK_DOC_USE_REBASE], [test -n "$GTKDOC_REBASE"])
])
