dnl Adds --with-libname-suffix configure option
dnl Usage: AG_ADD_OPTION_LIBNAME_SUFFIX
AC_DEFUN([AG_ADD_OPTION_LIBNAME_SUFFIX],
[
  AC_ARG_WITH(libname-suffix,
  AS_HELP_STRING([--with-libname-suffix],
     [build library as libmycode<suffix>.so]),
     [libname_suffix="${withval}"])

  AC_SUBST(FLU_LIBNAME_SUFFIX, "${libname_suffix}")
])
