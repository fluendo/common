dnl New toolchains have _gmtime defined in the header
dnl as a wrap to _gmtime32 or _gmtime64 instead of
dnl having it as external symbol.
dnl We are using some prebuilt binaries that depends
dnl of an external _gmtime definition.
dnl We need to alias in the linker _gmtime
dnl to the corresponding symbol in this architecture
dnl either _gmtime32 or _gmtime64
dnl Specifically, this symbol is needed by boost 1.55 at
dnl
dnl boost/date_time/c_time.hpp
dnl boost::date_time::c_time::\
dnl gmtime(const std::time_t* t, std::tm* result)
dnl
dnl and
dnl
dnl boost/date_time/microsec_time_clock.hpp
dnl boost::date_time::microsec_clock<boost::posix_time::ptime> \
dnl ::create_time(tm* (*)(long const*, tm*))

AC_DEFUN([FLU_CHECK_GMTIME],
[
  AC_MSG_CHECKING([if _gmtime needs alias])
  AC_LINK_IFELSE(
    [AC_LANG_PROGRAM([void* gmtime(const void *);],[gmtime(0);])],
      [AC_MSG_RESULT([no])],
      [
       if test "x$HAVE_CPU_X86_64" = "xyes" ; then
        CFLAGS="$CFLAGS -Wl,--defsym=_gmtime=_gmtime64"
        AC_MSG_RESULT([_gmtime64])
       else
        CFLAGS="$CFLAGS -Wl,--defsym=_gmtime=__gmtime32"
        AC_MSG_RESULT([_gmtime32])
       fi
      ])
])
