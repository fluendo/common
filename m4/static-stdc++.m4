dnl Configure option for linking libstc++ statically if wanted

AC_DEFUN([ARG_WITH_STATIC_LIBSTDCPP],
[
  AC_ARG_WITH([static-libstdc++],
    [AS_HELP_STRING([--with-static-libstdc++],
    [Statically link libstdc++/libgcc @<:@default=disabled@:>@])],
    [with_static_libstdcpp="yes"],
    [with_static_libstdcpp="no"])

  USE_STATIC_LIBSTDCPP=0
  dnl Strip out unnecessary dynamic linking in of libstdc++ and libgcc_s
  if test "x$with_static_libstdcpp" = "xyes";
  then
    USE_STATIC_LIBSTDCPP=1

    tmppdcxx=;
    for x in ${postdeps_CXX};
    do
      case $x in
        -lstdc++) true; ;;
        -lgcc_s) true; ;;
        *) tmppdcxx=${tmppdcxx}${tmppdcxx:+ }$x; ;;
      esac;
    done;
    postdeps_CXX="-lgcc_eh ${tmppdcxx}";
    AC_MSG_NOTICE([Cleaned libtool C++ postdeps: $postdeps_CXX])

    dnl Filter in libtool libstdc++ dependecy brought by other libraries
    sed -i '/deplibs=\$newdeplibs/{
    a         for filter in -lstdc\+\+ libstdc++\\.so libstdc++\\.dll -lgcc libgcc_s\\.so libgcc_s\\.dll
    a         do
    a           deplibs=\`echo \$deplibs | sed "s#[^ ]*\$filter[^ ]*##g"\`
    a         done
    a         deplibs=\"\$deplibs -static-libgcc -static-libstdc++ -l:libgcc.a -l:libgcc_eh.a -l:libstdc++.a -Wl,--exclude-libs -Wl,libgcc.a:libstdc++.a\"
    }' ltmain.sh

    STATIC_STDLIBS_DEPS="-static-libgcc -static-libstdc++ -l:libgcc.a -l:libstdc++.a -Wl,--exclude-libs -Wl,libgcc.a:libgcc_eh.a:libstdc++.a"
    AC_SUBST(STATIC_STDLIBS_DEPS)
  fi
  AC_SUBST(USE_STATIC_LIBSTDCPP)
  AM_CONDITIONAL([USE_STATIC_LIBSTDCPP], [test "x$with_static_libstdcpp" = "xyes"])
])
