
dnl This macros is intended to enable static and dynamic analyzers
AC_DEFUN([FLU_ANALYZERS],
[
  dnl enable clang's address sanitizer
  dnl https://clang.llvm.org/docs/AddressSanitizer.html
  AC_ARG_ENABLE(address-sanitizer,
    AC_HELP_STRING([--enable-address-sanitizer],
      [enable Clang's address sanitizer ]),
    [ADDRESS_SANITIZER=yes],
    [ADDRESS_SANITIZER=no]) dnl Default value

  if test "x$ADDRESS_SANITIZER" = xyes; then
    if test "x$compiler" != "xclang"; then
      AC_MSG_NOTICE(Build will use Clang Address Sanitizer)
    fi
    AS_COMPILER_FLAG(-fsanitize=address -fno-omit-frame-pointer -O1,
      CFLAGS="$CFLAGS -fsanitize=address -fno-omit-frame-pointer -O1")
  fi

  dnl enable clang's TheadSafetyAnalisys
  dnl https://clang.llvm.org/docs/ThreadSafetyAnalysis.html
  if test "x$compiler" == "xclang"; then
    AC_MSG_NOTICE(Build will use Clang Thread Safety Analysis)
    AS_COMPILER_FLAG(-Wthread-safety,
      CFLAGS="$CFLAGS -Wthread-safety")
  fi
])
