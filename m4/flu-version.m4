AC_DEFUN([FLU_EXPLODE_VERSION],
[
  if test -z $1; then
    FLU_VERSION="0.0.0.0"
  else
    FLU_VERSION=$1
  fi

  FLU_VERSION_MAJOR=[`echo $FLU_VERSION | cut -d '.' -f1`]
  if test "x$FLU_VERSION_MAJOR" = x; then
  FLU_VERSION_MAJOR=0
  fi
  AC_SUBST(FLU_VERSION_MAJOR)

  FLU_VERSION_MINOR=[`echo $FLU_VERSION | cut -d '.' -f2`]
  if test "x$FLU_VERSION_MINOR" = x; then
  FLU_VERSION_MINOR=0
  fi
  AC_SUBST(FLU_VERSION_MINOR)

  FLU_VERSION_MICRO=[`echo $FLU_VERSION | cut -d '.' -f3 | cut -d '-' -f1`]
  if test "x$FLU_VERSION_MICRO" = x; then
  FLU_VERSION_MICRO=0
  fi
  AC_SUBST(FLU_VERSION_MICRO)

  FLU_VERSION_PATCH=[`echo $FLU_VERSION | cut -d '.' -f4 | cut -d '-' -f1` ]
  if test "x$FLU_VERSION_PATCH" = x; then
  FLU_VERSION_PATCH=0
  fi
  AC_SUBST(FLU_VERSION_PATCH)
])
