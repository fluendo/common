AC_DEFUN([FLU_ARG_ENABLE_FORWARDABLE],
[
  pushdef([OPTION], translit([$1], [A-Z\_], [a-z\-]))
  pushdef([OPTION_UNDERSCORE], translit(OPTION, [a-z\-], [a-z\_]))
  pushdef([OPTION_UNDERSCORE_UPCASE], translit(OPTION, [a-z\-], [A-Z\_]))

  OPTION_UNDERSCORE[]_options=""
  OPTION_UNDERSCORE[]_enabled="no"

  AC_ARG_ENABLE(OPTION[],
    AS_HELP_STRING([--enable-[]OPTION@<:@=no/=conf_opts@:>@],
      [Enable 'OPTION' with provided configure options]),
    [
      if test "x${enableval}" != "xno"; then
        OPTION_UNDERSCORE[]_enabled="yes"
        dnl Do not set options when using --enable-xyz without '=arg'
        if test "x${enableval}" != "xyes"; then
            OPTION_UNDERSCORE[]_options=${enableval}
        fi
      fi
    ]
  )

  AC_SUBST([OPTION_UNDERSCORE])

  popdef([OPTION])
  popdef([OPTION_UNDERSCORE])
  popdef([OPTION_UNDERSCORE_UPCASE])
])
